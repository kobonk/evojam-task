# EvoJam's recruitment process task

It would be great if you could spend some time preparing a working version of the attached Dashboard panels. I'm looking to see how you approach the HTML / CSS and how you would implement the chart  (i.e., JS library, JSON data, etc.).
I would be interested to see how much you can achieve with minimal use of graphic (image) files. Compatibility with Chrome is fine for this exercise - don't worry about IE / Firefox, etc.
Finally, a short explanation of why you choose the technologies you choose would be very helpful. No great detail required, just some simple bullet points.
Let me know if you are willing to take this small task on. If yes, please put the answers on github or bitbucket with history of changes you made. 
