module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        src: 'src',
        build: 'build',
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> by <%= pkg.author.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            build: {
                src: '<%= src %>/js/*.js',
                dest: '<%= build %>/js/AMUDataDisplayApp.min.js'
            }
        },
        sass: {
            dist: {
                files: {
                    '<%= src %>/css/style.css': '<%= src %>/scss/style.scss'
                }
            }
        },
        cssmin: {
            dist: {
                options: {
                    banner: '/*! <%= pkg.name %> by <%= pkg.author.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
                },
                files: {
                    '<%= build %>/css/style.min.css': ['<%= src %>/css/style.css']
                }
            }
        },
        copy: {
            main: {
                files: [
                    {
                        src: '<%= src %>/index.html',
                        dest: '<%= build %>/index.html'
                    }
                ]
            },
            changedDataFiles: {
                expand: true,
                dot: true,
                cwd: '<%= src %>/data',
                src: ['**/*.*'],
                dest: '<%= build %>/data/'
            }
        },
        watch: {
            css: {
                files: '<%= src %>/scss/*.scss',
                tasks: ['sass', 'cssmin']
            },
            js: {
                files: '<%= src %>/js/*.js',
                tasks: ['uglify']
            },
            html: {
                files: '<%= src %>/index.html',
                tasks: ['copy:main']
            },
            json: {
                files: '<%= src %>/data/*.json',
                tasks: ['copy:changedDataFiles']
            }
        }
    });
    
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    
    grunt.registerTask('default', ['watch']);

};