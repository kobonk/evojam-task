var AMUDataDisplayParameters    = (function(){
    function Parameters(){
        var solutions   = null;
        
        this.getSolutions   = function(){
            if (!solutions) {
                var solutionsApp    = new AMUDataDisplayApp();
                var data            = solutionsApp.importJSONData("data/solutions.json", false);
                solutions      = data.values;
            }
            return solutions;
        };
    }
    return new Parameters();
})();

function AMUDataDisplayApp(){
    var that    = this;
    var xhr;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var jsonData;
    var parsedData;
    
    function parseData(){
        var solutions = null;
        var column = null;
        var xLabel = "";
        var rangeMax = 0;
        var rangeLimit = 1;
        var result  = {
            columns: [],
            colors: [],
            xLabels: [],
            yLabels: [],
            xAxisName: "",
            summary: {
                value: 0,
                unitName: "",
                unitSymbol: ""
            }
        };

        if (!parsedData) {
            if (jsonData) {
                solutions = AMUDataDisplayParameters.getSolutions();
                result.xAxisName = jsonData.description;
                result.summary = jsonData.summary;
                
                for (var c=0; c<solutions.length; c++) {
                    column  = [solutions[c].name];
                    result.colors.push(solutions[c].color);
                    for (var s=0; s<jsonData.solutions.length; s++) {
                        if (jsonData.solutions[s].id === solutions[c].id) {
                            for (var v=0; v<jsonData.solutions[s].weeklyValues.length; v++) {
                                xLabel    = "Week " + jsonData.solutions[s].weeklyValues[v].weekNumber;
                                if (result.xLabels.indexOf(xLabel) < 0) {
                                    result.xLabels.push(xLabel);
                                }
                                column.push(jsonData.solutions[s].weeklyValues[v].value);
                                if (rangeMax < jsonData.solutions[s].weeklyValues[v].value) {
                                    rangeMax    = jsonData.solutions[s].weeklyValues[v].value;
                                }
                            }
                            break;
                        }
                    }
                    result.columns.push(column);
                }
                while (rangeMax > rangeLimit) {
                    rangeLimit  = rangeLimit*10;
                }
                result.yLabels  = [0, rangeLimit/2, rangeLimit];
            }
            parsedData  = result;
        }
        return parsedData;
    }
    
    function convertNumber(numberToConvert){
        var integerPart = "";
        var decimalPart;
        var splittedParts;
        if (numberToConvert && !isNaN(numberToConvert) && numberToConvert >= 1000) {
            if (numberToConvert % 1 === 0) {
                integerPart = ""+numberToConvert;
            } else {
                integerPart = ""+(Math.floor(numberToConvert));
                decimalPart = ""+(numberToConvert % 1);
            }
            splittedParts = integerPart.split( /(?=(?:...)*$)/ );
            if (decimalPart) {
                return splittedParts.join(",")+"."+decimalPart;
            } else {
                return splittedParts.join(",");
            }
        }
        return ""+numberToConvert;
    }
    
    this.importJSONData = function(jsonUrl, async, callback){
        if (jsonUrl) {
            if (async) {
                xhr.open("GET", jsonUrl, true);
            } else {
                xhr.open("GET", jsonUrl, false);
            }
            xhr.onreadystatechange  = function(){
                if (xhr.readyState === 4 && xhr.status === 200) {
                    if (xhr.responseText) {
                        jsonData    = JSON.parse(xhr.responseText);
                        if (callback && callback.constructor instanceof Function) {
                            callback.call(that);
                        }
                    }
                }
            };
            xhr.send();
        }
        return jsonData;
    };
    
    this.generateChart  = function(DOMContainer, type){
        var chart = null;
        var chartParams = null;
        
        var summaryContainer = null;
        var detailsButton = null;
        var loopElement;
        
        var summaryElementNode = null;
        var summaryTextNode = null;
        
        if (!parsedData) {
            parseData();
        }
        
        if (DOMContainer) {
            loopElement = DOMContainer.nextElementSibling;
            while (loopElement) {
                if (loopElement.classList.contains("summary")) {
                    summaryContainer = loopElement;
                } else if (loopElement.tagName.toLowerCase() === "button") {
                    detailsButton = loopElement;
                }
                loopElement = loopElement.nextElementSibling;
            }
            
            if (summaryContainer && parsedData.summary) {
                if (parsedData.summary.unitSymbol) {
                    summaryElementNode = document.createElement("SUP");
                    summaryTextNode = document.createTextNode(parsedData.summary.unitSymbol);
                    summaryElementNode.appendChild(summaryTextNode);
                    summaryContainer.appendChild(summaryElementNode);
                }
                if (parsedData.summary.value) {
                    summaryTextNode = document.createTextNode(convertNumber(parsedData.summary.value));
                    summaryContainer.appendChild(summaryTextNode);
                }
                if (parsedData.summary.unitName) {
                    summaryElementNode = document.createElement("SMALL");
                    summaryTextNode = document.createTextNode(parsedData.summary.unitName);
                    summaryElementNode.appendChild(summaryTextNode);
                    summaryContainer.appendChild(summaryElementNode);
                }
            }
        }
        
        if (parsedData) {
            chartParams = {
                bindto: DOMContainer,
                data: {
                    columns: parsedData.columns,
                    type: type
                },
                grid: {
                    y: {
                        show: true
                    }
                },
                point: {
                    r: 5
                }
            };
            if (parsedData.colors) {
                chartParams.color = {
                    pattern: parsedData.colors
                };
            }
            if (parsedData.xLabels || parsedData.yLabels) {
                chartParams.axis = {};
                if (parsedData.xLabels) {
                    chartParams.axis.x = {
                        type: "category",
                        categories: parsedData.xLabels,
                        tick: {
                            centered: true
                        }
                    };
                }
                if (parsedData.yLabels) {
                    chartParams.axis.y = {
                        tick: {
                            count: 3,
                            values: parsedData.yLabels
                        }
                    };
                }
            }
            
            chartParams.oninit = function(){
                var chartDescription = null;
                var descriptionText = null;
                
                var svg = DOMContainer.getElementsByTagName("svg")[0];
                
                var legend = null;
                var legendTile = null;
                var attribute = null;
                
                if (parsedData.xAxisName) {
                    chartDescription = document.createElement("LABEL");
                    descriptionText = document.createTextNode(parsedData.xAxisName);
                    chartDescription.appendChild(descriptionText);
                }

                if (svg) {
                    if (chartDescription) {
                        DOMContainer.insertBefore(chartDescription, svg);
                    }
                    
                    legend = svg.lastElementChild;
                    if (legend) {
                        for (var l=0; l<legend.childNodes.length; l++) {
                            legendTile = legend.childNodes[l].getElementsByClassName("c3-legend-item-tile")[0];
                            if (legendTile) {
                                attribute = document.createAttribute("ry");
                                attribute.value = 5;
                                legendTile.setAttributeNode(attribute);
                                attribute = document.createAttribute("rx");
                                attribute.value = 5;
                                legendTile.setAttributeNode(attribute);

                                legendTile.setAttribute("width", 7);
                                legendTile.setAttribute("height", 7);
                                legendTile.setAttribute("y", 1);
                            }
                        }
                    }
                }
                
                
            };
            
            chart = c3.generate(chartParams);
            
            
            
        }
    };
    
}